# nfs-pv

## Goal

Mount a legacy NFS share into a K8s pod via persistent volume claim

## Prerequisite

- A Kubernetes cluster
- A legacy NFS server on the same network as the worker nodes of your K8s cluster.
I used server share 192.168.1.220:/mnt/share.  This is a old computer with a USB drive shared via NFS.  Please test the mounting of this share on the worker nodes of your K8s cluster and install any pre-requisites like nfs-utils etc on the worker nodes else you may see an error like the one in the pitfalls section.

## File descriptions

[nfs-pv](nfs-pv/nfs-pv.yaml)    creates a persistent volume  
[nfs-pvc](nfs-pv/nfs-pvc.yaml)  creates a persistent volume claim  
[pod-nfs](nfs-pv/pod-nfs.yaml)  mounts the PVC into the pod and writes to it...  

## Process

1. Create the PV  
`kubectl create -f https://gitlab.com/BilalShahWork/mykubernetes/-/raw/main/nfs-pv/nfs-pv.yaml`

2. Create the PVC  
`kubectl create -f https://gitlab.com/BilalShahWork/mykubernetes/-/raw/main/nfs-pv/nfs-pvc.yaml`

3. Create the replication controlle rwhich will control 1 pod that mounts this PVC and writes to it  
`kubectl create -f https://gitlab.com/BilalShahWork/mykubernetes/-/raw/main/nfs-pv/pod-nfs.yaml`

4. Observe the output on the NFS server:  
`tail -f /mnt/share/index.html`

5. Scale it all the way to 110 pods, the max on a node ....worked fine for me :)

## Pitfalls

- ERROR

        (combined from similar events): MountVolume.SetUp failed for volume "nfs" : mount failed: exit status   32
        Mountingcommand:   systemd-run Mounting arguments: --description=Kubernetes transient mount for /var/lib/
        kubeletpodse3afb7cd-7188-470d-9461-f3a2f27178d8/volumes/kubernetes.io~nfs/nfs --scope -- mount -t nfs 192.168.1.220:/mntshare 
        /var/lib/kubelet/pods/e3afb7cd-7188-470d-9461-f3a2f27178d8/volumes/kubernetes.io~nfs/nfs 
        Output: Runningscope   as   unitrun-12919.scope. mount: wrong fs type, bad option, bad superblock 
        on 192.168.1.220:/mnt/share,missing   codepage   orhelper program, or other error (for several filesystems 
        (e.g. nfs, cifs) you might need a sbin/mount.type   helperprogram) In some cases useful info 
        is found in syslog - try dmesg | tail or so.
  
- Forum that helped to resolve this error  
[https://stackoverflow.com/questions/34113569/kubernetes-nfs-volume-mount-fail-with-exit-status-32]
  
- Summary Resolution: installed nfs-utils on worker nodes (as root)

## References

[1] installing an extra hard disk (USB drive) [https://ep.gnt.md/index.php/how-to-add-a-new-disk-in-centos-7/]  
[2] installing an NFS server and client [https://www.itzgeek.com/how-tos/linux/centos-how-tos/how-to-setup-nfs-server-on-centos-7-rhel-7-fedora-22.html]  
[3] Tutorial 1 [https://cloud.netapp.com/blog/kubernetes-nfs-two-quick-tutorials-cvo-blg]  
[4] Tutorial 2 [https://cloud.netapp.com/blog/kubernetes-persistent-storage-why-where-and-how]  
[5] Installing NFS as a PV and PVC [https://github.com/kubernetes/examples/tree/master/staging/volumes/nfs]  
[6] How to mount NFS into pod [https://cloud.netapp.com/blog/kubernetes-nfs-two-quick-tutorials-cvo-blg]  
