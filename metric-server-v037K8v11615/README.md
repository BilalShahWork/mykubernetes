# metric-server-v037K8v11615

## Goal

install metric-server v0.3.7 on Kubernetes v1.16.15 and not have to make the post install adjustments

## Prerequisite

- kubernetes cluster at v1.16.15

## File descriptions

[components.yaml](metric-server-v037K8v11615/components.yaml) this is the edited file - please see references  

## Process

### Installation

`kubectl apply -f https://gitlab.com/BilalShahWork/mykubernetes/-/raw/main/metric-server-v037K8v11615/components.yaml`

### Removal

?

## Pitfalls

- removal could be interesting :)

## References

[1] post installation issues and proposed fixes mentioned here: [BilalShahWork/mykubernetes#2]  
[2] fixes applied and new file stored [here](metric-server-v037K8v11615/components.yaml)  
