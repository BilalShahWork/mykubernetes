# mykubernetes

## nfs-pv

Mount a legacy NFS share into a K8s pod via persistent volume claim

## dashboard

Install the kubernetes dashboard

## LENS - The Kubernetes Dashboard

installation [BilalShahWork/mykubernetes#1]

## metric-server-v037K8v11615

install metric-server v0.3.7 on Kubernetes v1.16.15 and not have to make the post install adjustments

## issues that dont have a specific folder, only tickets here

1. recovering n228, namespace stuck in terminating state [BilalShahWork/mykubernetes#4]
2. [https://gitlab.com/BilalShahWork/mykubernetes/-/issues/4#note_657125940]
