# dashboard-adminuser

## Goal

Create sample user as defined in the kubernetes official definition [2]

## Prerequisite

- A Kubernetes cluster
- dashboard already installed as per the official kubernetes dashboard instructions [1]

## File descriptions

## Process

1. apply this file  
`kubectl apply -f https://gitlab.com/BilalShahWork/mykubernetes/-/raw/main/dashboard/dashboard-adminuser.yaml`
2. follow along the official documentation here [1]  

## Pitfalls

## References

- [1] <https://github.com/kubernetes/dashboard>  
- [2] <https://github.com/kubernetes/dashboard/blob/master/docs/user/access-control/creating-sample-user.md>  
